const mysql = require("mysql");

let connection = mysql.createConnection({
    host: 'localhost',
    port: '8889',
    user: 'root',
    password: 'root',
    database: 'mirasim.com'
});

connection.connect(function (err) {
    if (err) throw err;
    console.log('bağlantı başarılı.')
});
module.exports = connection;
