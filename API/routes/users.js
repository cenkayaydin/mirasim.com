var express = require('express');
var router = express.Router();
var con = require("../bin/connection");

//kullanıcı kayıt
router.post('/register', function (req, res) {
    const registerData = {
        package_id: req.body.package_id,
        full_name: req.body.full_name,
        tc_number: req.body.tc_number,
        password: req.body.password,
        email: req.body.email,
        birthday: req.body.birthday
    };

    let sorgu = 'INSERT INTO users VALUES (null, ?, ?, ?, ?, ?, ?)';

    con.query(sorgu, [registerData.package_id, registerData.full_name, registerData.tc_number, registerData.password, registerData.email, registerData.birthday], (err, results) => {
        if (err) throw err;
        res.jsonp({
            data: results
        }, 200)
    });
});


//kullanıcı detayı getirme
router.get('/:id/detail', (req, res) => {
    const id = req.params.id;

    let sorgu = 'SELECT u.*, p.* FROM users AS u INNER JOIN packages AS p ON u.id = ? AND u.package_id = p.id';

    con.query(sorgu, [id], (err, results) => {
        if (err) throw err;
        res.jsonp({
            userDetail: results[0]
        }, 200)
    })

});

//kullanıcı giriş

router.post('/login', (req, res) => {
    const loginData = {
        email: req.body.email,
        password: req.body.password
    };
    let sorgu = 'SELECT * FROM users WHERE email = ? AND password = ?';
    con.query(sorgu, [loginData.email, loginData.password], (err, results) => {
        if (err || Object.keys(results).length <= 0) {
            res.jsonp({
                data: 'no user for this information'
            }, 401)
        } else {
            res.jsonp({
                data: results
            }, 200)
        }
    })
});

module.exports = router;
