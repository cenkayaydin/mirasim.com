var express = require('express');
var router = express.Router();
var con = require("../bin/connection");

//crete hertages

var date = new Date().toString().slice(0, 15);

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [year, month, day].join('-');
}

router.post('/create', (req, res) => {
    const createHertagesData = {
        user_id: req.body.user_id,
        hertage_header: req.body.hertage_header,
        hertage_text: req.body.hertage_text,
        created_at: formatDate(date),
        modify_at: formatDate(date)
    };

    let sorgu = 'INSERT INTO hartages VALUES (null, ?, ?, ?, ?, ?)';
    con.query(sorgu, [createHertagesData.user_id, createHertagesData.hertage_header, createHertagesData.hertage_text, createHertagesData.created_at, createHertagesData.modify_at], (err, results) => {
        if (err) throw err;
        res.jsonp({
            data: req.body
        }, 200)
    })
});

//hertage update

router.post('/update', (req, res) => {
    const updateHertageData = {
        hertage_header: req.body.hertage_header,
        hertage_text: req.body.hertage_text,
        modify_at: formatDate(date),
        hertage_id: req.body.hertage_id,
    };
    let sorgu = 'UPDATE hartages SET hertage_header = ?, hertage_text = ?, modify_at = ? WHERE id = ?';
    con.query(sorgu, [updateHertageData.hertage_header, updateHertageData.hertage_text, updateHertageData.modify_at, updateHertageData.hertage_id], (err, results) => {
        if (err) throw err;
        res.jsonp({
            data: results
        }, 200)
    })
});

//hertage delete

router.get('/delete/:hertage_id', (req, res) => {
    const id = req.params.hertage_id;
    let sorgu = 'DELETE FROM hartages WHERE id = ?';
    con.query(sorgu, [id], (err, results) => {
        if (err) throw err;
        res.jsonp({
            data: results
        }, 200)
    })
});

//user hertages all
router.get('/:user_id/hertages-show', (req, res) => {
    const id = req.params.user_id;
    let sorgu = 'SELECT * FROM hartages WHERE user_id = ?';
    con.query(sorgu, [id], (err, results) => {
        if (err) throw err;
        res.jsonp({
            data: results
        }, 200)
    })
});

module.exports = router;
