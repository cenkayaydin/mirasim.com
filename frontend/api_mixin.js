import axios from "axios"
import Vue from "vue"

let config = {
    headers: {
        'Content-Type': 'application/json',
    },
};

const api_mixin = {
    data: function () {
        return {
            baseUrl: process.env.NODE_ENV === 'production' ? '' : "http://localhost:3000/"
        }
    },
    methods: {
        api_calling() {
            let service = axios.create();
            service.interceptors.response.use(this.handleSuccess());
            return service;
        },
        handleSuccess(response) {
            return response;
        },
        api_get(path, getSuccess, getError) {
            this.api_calling().get(this.baseUrl + path, config).then(
                (response) => (getSuccess(response))
            ).catch(
                (error) => (getError(error))
            );
        },
        api_post(path, payload, postSuccess, postError) {
            this.api_calling().post(this.baseUrl + path, payload, config).then(
                (response) => (postSuccess(response.data))
            ).catch(
                (error) => (postError(error))
            )
        },
        handleAlert(title, text, type) {
            Vue.notify({
                title: title,
                text: text,
                type: type,
            });
        }
    }
};

export default api_mixin;
