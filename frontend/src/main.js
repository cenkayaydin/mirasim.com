import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import axios from "axios";

import api_mixin from "../api_mixin.js";

Vue.config.productionTip = false;
Vue.mixin(api_mixin);


new Vue({
    router,
    store,
    axios,
    mixins: [api_mixin],
    render: h => h(App)
}).$mount('#app');
