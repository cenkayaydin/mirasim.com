# mirasim.com

#### API Documentation

Kullanıcı Olayları:

    register:(POST)
	url = users/register
	params = package_id(Number), full_name, tc_number(Number), password, email, birthday
	
	user detail:(GET)
	url= users/:user_id/detail
	
	login:(POST)
	params: email, password
	url= users/login

Miras Olayları:

    miras ekleme:(POST)
	url = hertages/create
	params = user_id(Number), hertage_header, hertage_text,
	
	miras günelleme:(POST)
	url = hertages/update
	params = hertage_header, hertage_text, hertage_id(Number)
	
	miras silme:(GET)
	url = hertages/delete/:hertage_id

